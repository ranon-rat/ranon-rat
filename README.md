<h1  align="center">Hello I am ranon-rat</h1>

<h2 align ="center"><a href="#aboutMe">  about me </a>|<a href="#projects">  projects</a>|<a href="#stats"> stats </a>|<a href="#myFriends">  my friends</a> |<a href="https://discord.gg/e52RFh7Cg2"> my discord server</a></h2>


<h2 align = "center"> 
I program in these languages </h2>
<p align="center">
  <a target="_blank" rel="noopener noreferrer" href="https://camo.githubusercontent.com/b19864f800e20ca559cd76b53f377ef65249119ce7a8da98becc200f6ef56e30/68747470733a2f2f7365656b6c6f676f2e636f6d2f696d616765732f4e2f6e6f64656a732d6c6f676f2d464245313232453337372d7365656b6c6f676f2e636f6d2e706e67"><img height="150" src="https://camo.githubusercontent.com/b19864f800e20ca559cd76b53f377ef65249119ce7a8da98becc200f6ef56e30/68747470733a2f2f7365656b6c6f676f2e636f6d2f696d616765732f4e2f6e6f64656a732d6c6f676f2d464245313232453337372d7365656b6c6f676f2e636f6d2e706e67" data-canonical-src="https://seeklogo.com/images/N/nodejs-logo-FBE122E377-seeklogo.com.png" style="max-width:100%;"></a>
    <a target="_blank" rel="noopener noreferrer" href="https://camo.githubusercontent.com/9255dba4a9ad5a906afd63a77b2d3498cbd7fa527008a417968683f5e8e545b2/68747470733a2f2f75706c6f61642e77696b696d656469612e6f72672f77696b6970656469612f636f6d6d6f6e732f7468756d622f342f34632f547970657363726970745f6c6f676f5f323032302e7376672f3132303070782d547970657363726970745f6c6f676f5f323032302e7376672e706e67"><img height="150" src="https://camo.githubusercontent.com/9255dba4a9ad5a906afd63a77b2d3498cbd7fa527008a417968683f5e8e545b2/68747470733a2f2f75706c6f61642e77696b696d656469612e6f72672f77696b6970656469612f636f6d6d6f6e732f7468756d622f342f34632f547970657363726970745f6c6f676f5f323032302e7376672f3132303070782d547970657363726970745f6c6f676f5f323032302e7376672e706e67" data-canonical-src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Typescript_logo_2020.svg/1200px-Typescript_logo_2020.svg.png" style="max-width:100%;"></a>
  <a target="_blank" rel="noopener noreferrer" href="https://camo.githubusercontent.com/82a5f91b3c5f8ff699f9a79ef46a81b3c7800d7e8e63651b1a75810f24106b0e/68747470733a2f2f63646e2e646973636f72646170702e636f6d2f656d6f6a69732f3739353534343133343431363732383036352e676966"><img height="150" src="https://camo.githubusercontent.com/82a5f91b3c5f8ff699f9a79ef46a81b3c7800d7e8e63651b1a75810f24106b0e/68747470733a2f2f63646e2e646973636f72646170702e636f6d2f656d6f6a69732f3739353534343133343431363732383036352e676966" data-canonical-src="https://cdn.discordapp.com/emojis/795544134416728065.gif" style="max-width:100%;"></a>
  <a target="_blank" rel="noopener noreferrer" href="https://camo.githubusercontent.com/5ff8c4958c84d260a95ab0a2413c37728b9f43c25c5f82e20ca9c0918a76e84d/68747470733a2f2f75706c6f61642e77696b696d656469612e6f72672f77696b6970656469612f636f6d6d6f6e732f7468756d622f312f31382f49534f5f432532422532425f4c6f676f2e7376672f3132303070782d49534f5f432532422532425f4c6f676f2e7376672e706e67"><img height="150" src="https://camo.githubusercontent.com/5ff8c4958c84d260a95ab0a2413c37728b9f43c25c5f82e20ca9c0918a76e84d/68747470733a2f2f75706c6f61642e77696b696d656469612e6f72672f77696b6970656469612f636f6d6d6f6e732f7468756d622f312f31382f49534f5f432532422532425f4c6f676f2e7376672f3132303070782d49534f5f432532422532425f4c6f676f2e7376672e706e67" data-canonical-src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/1200px-ISO_C%2B%2B_Logo.svg.png" style="max-width:100%;"></a>
  <a target="_blank" rel="noopener noreferrer" href="https://camo.githubusercontent.com/bf64345adc6adfb2cb30950f35c2f1f1ef08a64c4df1b44426655f9906981998/68747470733a2f2f63646e2e646973636f72646170702e636f6d2f656d6f6a69732f3739363139353737333037343633363830312e6769663f763d31"><img height="150" src="https://camo.githubusercontent.com/bf64345adc6adfb2cb30950f35c2f1f1ef08a64c4df1b44426655f9906981998/68747470733a2f2f63646e2e646973636f72646170702e636f6d2f656d6f6a69732f3739363139353737333037343633363830312e6769663f763d31" data-canonical-src="https://cdn.discordapp.com/emojis/796195773074636801.gif?v=1" style="max-width:100%;"></a>
</p>

---

<h1 id="aboutMe"> about me</h1>
<h4> I am a programmer of 15 years old i really like coding, you gonna see creative things like a music generator or discord bot but. is not the only thing i do , sometimes i like to make tcp servers or web pages .</h4>
<img src="https://komarev.com/ghpvc/?username=ranon-rat" alt="blueedge"/>

---

<h1 id ="projects"> my best projects in my github they are </h1>
<p align="center">                 
<a href="https://github.com/pythonBoy123/redditReplaceHumans">
  <img height=120 src="https://github-readme-stats.vercel.app/api/pin/?username=ranon-rat&repo=redditReplaceHumans&show_owner=true&theme=tokyonight"></a>
 </a>
<a href="https://github.com/ranon-rat/neuralTextGenerator">
   <img height=120 src="https://github-readme-stats.vercel.app/api/pin/?username=ranon-rat&repo=neuralTextGenerator&show_owner=true&theme=tokyonight">
 </a>
<a href="https://github.com/ranon-rat/FractalsGolang">
   <img height=120 src="https://github-readme-stats.vercel.app/api/pin/?username=ranon-rat&repo=FractalsGolang&show_owner=true&theme=tokyonight">
 </a>
<a href="https://github.com/ranon-rat/echo-server-go"
  <img height=120 src="https://github-readme-stats.vercel.app/api/pin/?username=ranon-rat&repo=echo-server-go&show_owner=true&theme=tokyonight">
 </a>
<a href="https://github.com/ranon-rat/when-haces-tus-momos-en-consola">
  <img height=120 src="https://github-readme-stats.vercel.app/api/pin/?username=ranon-rat&repo=when-haces-tus-momos-en-consola&show_owner=true&theme=tokyonight">
</a>
<a href="https://github.com/ranon-rat/golang-remote">
  <img height=120 src="https://github-readme-stats.vercel.app/api/pin/?username=ranon-rat&repo=golang-remote&show_owner=true&theme=tokyonight">
  </a>
  <a href="https://github.com/ranon-rat/sayBruh">
  <img height=120 src="https://github-readme-stats.vercel.app/api/pin/?username=ranon-rat&repo=sayBruh&show_owner=true&theme=tokyonight">
  </a>
  </p>

<h1 id ="stats"> stats</h1>

<p>
<img height=150 src="https://github-readme-stats.vercel.app/api/top-langs/?username=ranon-rat&layout=compact&theme=tokyonight&hide=html">
<img height=150 src="https://github-readme-stats.vercel.app/api?username=ranon-rat&count_private=true&show_icons=true&theme=tokyonight">
</p>

<img src="https://komarev.com/ghpvc/?username=ranon-rat">

<h1 id="myFriends"> ⭐️ My friends ⭐️</h1>

- <h2><a href="https://gitlab.com/ELPanaJose1"> ⭐️ ElPanaJose ⭐️</a></h2>

  - <h2>About him</h2>

    - He is a junior developer in javascript,typescript, python and golang. He did <a href="https://chat-pai.herokuapp.com/"> chatPai </a>

    | Projects that i made with ElPanaJose                            |
    | --------------------------------------------------------------- |
    | <a href="https://chat-pai.herokuapp.com/"> ⭐️ chatPai ⭐️ </a> |
    | <a href="https://github.com/ELPanaJose/BotJose"> botJose</a>    |
    | <a href="https://github.com/ranon-rat/sayBruh">sayBruh</a>      |

- <h2><a href="https://github.com/RustyBalboadev"> RustyBalboaDev</a> </h2>

  - <h3> About him</h3>

    - He is a senior python and golang developer. He did <a href="https://yoink.rip/">yoink.rip</a>

  - <h3> Project that i made with RustyBalboaDev</h3>

    - <a href="https://github.com/ranon-rat/echo-server-go">echo server go</a>
    
    ---
 <h2 align ="center" >I know</h2>
<p align="center" >
<center>

| programming languages                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           | markup languages                                                                                                                                                                                                                                                                                                                                                                        | databases                                                                                                                                                                                                                                                                                                                                          | routers                                                                                                                                                                    |
| ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| <img height=30 src= "https://seeklogo.com/images/N/nodejs-logo-FBE122E377-seeklogo.com.png"><img height=30 src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Typescript_logo_2020.svg/1200px-Typescript_logo_2020.svg.png"><img height=30  src="https://cdn.discordapp.com/emojis/795544134416728065.gif"><img height=30 src = "https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/1200px-ISO_C%2B%2B_Logo.svg.png"><img height=30 src="https://cdn.discordapp.com/emojis/796195773074636801.gif?v=1"> | <img height = 30 src="https://image.flaticon.com/icons/png/512/1216/1216733.png"><img height = 30 src="https://cdn.pixabay.com/photo/2017/08/05/11/16/logo-2582747_1280.png"><img height=30 src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/Markdown-mark.svg/1280px-Markdown-mark.svg.png"> <img height=30 src="https://image.flaticon.com/icons/png/512/29/29611.png"> | <img height=30 src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/SQLite370.svg/1200px-SQLite370.svg.png"> <img height=30 src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Postgresql_elephant.svg/1200px-Postgresql_elephant.svg.png"> <img height =30 src="https://download.logo.wine/logo/MySQL/MySQL-Logo.wine.png"> | <img height=30 src="https://miro.medium.com/max/400/1*dQnbv0c36h6ijsocaRWksQ.png"> <img height=30 src="https://www.sourcefuse.com/wp-content/uploads/2018/11/express.png"> |

</center>
</p>
